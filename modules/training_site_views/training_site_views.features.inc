<?php
/**
 * @file
 * training_site_views.features.inc
 */

/**
 * Implements hook_views_api().
 */
function training_site_views_views_api() {
  return array("api" => "3.0");
}
