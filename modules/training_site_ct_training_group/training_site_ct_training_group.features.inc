<?php
/**
 * @file
 * training_site_ct_training_group.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function training_site_ct_training_group_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "ds" && $api == "ds") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function training_site_ct_training_group_node_info() {
  $items = array(
    'training_group' => array(
      'name' => t('Training Group'),
      'base' => 'node_content',
      'description' => t('A training group is a way to offer custom sets of available courses to individual groups, such as organizations or associations with members interested in your courses.'),
      'has_title' => '1',
      'title_label' => t('Group name'),
      'help' => '',
    ),
  );
  return $items;
}
