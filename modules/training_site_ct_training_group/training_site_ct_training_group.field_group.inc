<?php
/**
 * @file
 * training_site_ct_training_group.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function training_site_ct_training_group_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group__training_group_tabset|node|training_group|form';
  $field_group->group_name = 'group__training_group_tabset';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'training_group';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Training Group Info',
    'weight' => '1',
    'children' => array(
      0 => 'group_training_group_courses_tab',
      1 => 'group_training_group_logo_tab',
      2 => 'group_training_group_desc_tab',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group--training-group-tabset field-group-htabs ',
      ),
    ),
  );
  $export['group__training_group_tabset|node|training_group|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_training_group_courses_tab|node|training_group|form';
  $field_group->group_name = 'group_training_group_courses_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'training_group';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group__training_group_tabset';
  $field_group->data = array(
    'label' => 'Courses',
    'weight' => '5',
    'children' => array(
      0 => 'field_training_group_courses',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-training-group-courses-tab field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_training_group_courses_tab|node|training_group|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_training_group_desc_tab|node|training_group|form';
  $field_group->group_name = 'group_training_group_desc_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'training_group';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group__training_group_tabset';
  $field_group->data = array(
    'label' => 'Description',
    'weight' => '4',
    'children' => array(
      0 => 'body',
      1 => 'title',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-training-group-desc-tab field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_training_group_desc_tab|node|training_group|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_training_group_logo_tab|node|training_group|form';
  $field_group->group_name = 'group_training_group_logo_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'training_group';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group__training_group_tabset';
  $field_group->data = array(
    'label' => 'Logo',
    'weight' => '6',
    'children' => array(
      0 => 'field_training_group_logo',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-training-group-logo-tab field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_training_group_logo_tab|node|training_group|form'] = $field_group;

  return $export;
}
