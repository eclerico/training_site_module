<?php
/**
 * @file
 * training_site_ct_training_group.ds.inc
 */

/**
 * Implements hook_ds_layout_settings_info().
 */
function training_site_ct_training_group_ds_layout_settings_info() {
  $export = array();

  $ds_layout = new stdClass();
  $ds_layout->api_version = 1;
  $ds_layout->id = 'node|training_group|default';
  $ds_layout->entity_type = 'node';
  $ds_layout->bundle = 'training_group';
  $ds_layout->view_mode = 'default';
  $ds_layout->layout = 'panels-twocol_stacked';
  $ds_layout->settings = array(
    'regions' => array(
      'left' => array(
        0 => 'field_training_group_logo',
      ),
      'right' => array(
        1 => 'body',
      ),
      'bottom' => array(
        2 => 'training_partner_page_courses_eva_entity_view_1',
      ),
    ),
    'fields' => array(
      'field_training_group_logo' => 'left',
      'body' => 'right',
      'training_partner_page_courses_eva_entity_view_1' => 'bottom',
    ),
    'classes' => array(),
    'wrappers' => array(
      'top' => 'div',
      'left' => 'div',
      'right' => 'div',
      'bottom' => 'div',
    ),
    'layout_wrapper' => 'div',
    'layout_attributes' => '',
    'layout_attributes_merge' => 1,
    'layout_link_attribute' => '',
    'layout_link_custom' => '',
  );
  $export['node|training_group|default'] = $ds_layout;

  return $export;
}
