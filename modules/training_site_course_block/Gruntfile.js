'use strict';

module.exports = function(grunt) {
	grunt.initConfig({
		sass: {
			dist: {
				files: {
					'app/styles/styles.css' : 'app/styles/*.scss',
				}
			},
		},
		watch: {
			compileSass: {
				files: 'app/styles/*.scss',
				tasks: ['sass'],
				options: {
					livereload: {
						port: 9000,
					}
				}
			},
		},

	});

	grunt.loadNpmTasks('grunt-contrib-watch');
	grunt.loadNpmTasks('grunt-contrib-sass');

	grunt.registerTask('default', ['watch']);
}