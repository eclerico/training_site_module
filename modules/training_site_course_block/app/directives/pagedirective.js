'use strict';

angular.module('trainingSiteCourseBlockApp')
.directive('pageTemplate', function($compile) {

	var pageTemplate = '';
	
	pageTemplate += '<div offset="0" class="course-info"><div>';

	// pageTemplate += '<div class="mini-header">';

	pageTemplate += 'Simplicity <span>Financial Wellness Program</span>';

	// pageTemplate += '</div>'; // div class="mini-header"

	pageTemplate += '<h1 ng-bind-html="course.title"></h1>';

	pageTemplate += '<ul> \
	<li class="progess-page" ng-repeat="n in [] | range:page_count" ng-class="{selected: n==(page_num-1)}"></li> \
	</ul> \
	';

	pageTemplate += '<h2 ng-bind-html="page.title"></h2>';

	pageTemplate += '</div></div>'; // end course-info

	pageTemplate += '<div class="course-page">';

	pageTemplate += '<div ng-bind-html="page.field_content"></div>';

	pageTemplate += '<div class="page-navigation">';

	pageTemplate += '<a ng-if="api.isNext()" ng-click="go(1)" ui-sref="home({course: api.course(), id: api.next()})" class="next"><span>Next ></span></a>';

	pageTemplate += '<a ng-if="api.isPrev()" ng-click="go(-1)" ui-sref="home({course: api.course(), id: api.prev()})" class="prev"><span>< Prev</span></a>';

	pageTemplate += '<a ng-if="api.isNext() === false" href="{{quizurl}}" class="next">Take the quiz!</a>';

	pageTemplate += '</div>';

	pageTemplate += '</div>';
    return {
      restrict: 'E',
      template: pageTemplate,
    };

  });