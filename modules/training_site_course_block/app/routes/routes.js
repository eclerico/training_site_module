var Routes = angular.module('Routes', []);

Routes.config(['$routeProvider',
    function($routeProvider) {
        $routeProvider
            .when('/', { // right below here needs to change to reflect prod when that time comes
                templateUrl: '/profiles/training_site/modules/custom/training_site_module/modules/training_site_course_block/app/partials/home.html',
                controller: 'CourseController'
            });
    }
]);