'use strict';

angular.module('trainingSiteCourseBlockApp')
  .controller('MainCtrl', function($rootScope, $scope, loadedPages, stateParams, $location) {
    
    $scope.pages = loadedPages[0].pages;
    $scope.page = loadedPages[0].pages[(stateParams.id - 1)]; // correct for better human paths here
    $scope.course = loadedPages[0].course;
    $scope.page_count = loadedPages[0].course.page_count;
    $scope.page_num = stateParams.id;
    $scope.quizurl = $location.protocol() + '://' + $location.host() + ':' + $location.port() + '/node/' + loadedPages[0].course.quiz_ref + '/take'; 

    $rootScope.go = function(count) {
        if(count > 0) {
          $location.hash('#top');
        } else {
          $location.hash('#top');  
        }
      }

    $scope.api = {

      course: function() {
        return parseInt(stateParams.course, 10);
      },
      next: function() {
        return parseInt(stateParams.id, 10) + 1;
      },
      prev: function() {
        return parseInt(stateParams.id, 10) - 1;
      },
      isNext: function() {
        return (parseInt(stateParams.id, 10) < parseInt($scope.page_count, 10));
      },
      isPrev: function() {
        return (parseInt(stateParams.id, 10) > 1);
      }
    };

    $rootScope.$on('$stateChangeStart', 
    function(event, toState, toParams, fromState, fromParams){ 
      if (parseInt(toParams.id, 10) === 0 || parseInt(toParams.id, 10) > $scope.page_count) {
        event.preventDefault(); 
      }
      //console.log(toParams.id);
      //console.log($scope);
      

    // transitionTo() promise will be rejected with 
    // a 'transition prevented' error
    });


  });
