'use strict';

angular.module('trainingSiteCourseBlockApp', [
  'ngAnimate',
  'restangular',
  'ngResource',
  'ngSanitize',
  'ui.router',
  'sticky'
 ])
.config(function($stateProvider, $urlRouterProvider) {

    $urlRouterProvider.otherwise('/not-found');

    $stateProvider
      .state('home', {
        url: '/{course:[0-9]{1,9}}/page/{id:[0-9]{1,9}}',
        template: '<div class="pages"><page-template></page-template></div>',
        controller: 'MainCtrl',
        resolve: {
          loadedPages: function(RestPages, $stateParams) {
            return RestPages.getPages().one('retrieve/' + $stateParams.course).get()
            .then(function (pages) {
              return pages;
            });
          },
          stateParams: function($stateParams) {
            return $stateParams;
          },
        },
      })
      .state('not-found', {
        url: '/not-found',
        template: 'Not found.'
      });
  });