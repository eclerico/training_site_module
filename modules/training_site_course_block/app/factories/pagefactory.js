'use strict';

angular.module('trainingSiteCourseBlockApp')
  .factory('RestPages', function(Restangular, $location) {
    // Service logic
    // ...
    var svc = {};

    svc.getPages = function() {
      return Restangular.allUrl('pages', $location.protocol() + '://' + $location.host() + ':' + $location.port() + '/api/course/');
    };
    // Public API here
    return svc;

  });

