<?php
/**
 * @file
 * training_site_certificate_content_type.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function training_site_certificate_content_type_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function training_site_certificate_content_type_node_info() {
  $items = array(
    'certificate_of_completion' => array(
      'name' => t('Certificate of Completion'),
      'base' => 'node_content',
      'description' => t('Auto created when a quiz is passed by a member. This node displays as a PDF download of the official Certificate of Completion.'),
      'has_title' => '1',
      'title_label' => t('Course title'),
      'help' => '',
    ),
  );
  return $items;
}
