<?php
/**
 * @file
 * training_site_rules_config.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function training_site_rules_config_default_rules_configuration() {
  $items = array();
  $items['rules_training_site_pass_quiz'] = entity_import('rules_config', '{ "rules_training_site_pass_quiz" : {
      "LABEL" : "Training Site Pass Quiz",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Training site" ],
      "REQUIRES" : [ "rules", "quiz_rules" ],
      "ON" : [ "quiz_rules_quiz_passed" ],
      "IF" : [
        { "entity_is_of_bundle" : {
            "entity" : [ "node" ],
            "type" : "node",
            "bundle" : { "value" : { "quiz" : "quiz" } }
          }
        }
      ],
      "DO" : [
        { "entity_fetch" : {
            "USING" : { "type" : "node", "id" : [ "node:nid" ] },
            "PROVIDE" : { "entity_fetched" : { "quiz_entity_fetched" : "Fetched quiz entity" } }
          }
        },
        { "entity_create" : {
            "USING" : {
              "type" : "node",
              "param_type" : "certificate_of_completion",
              "param_title" : "[node:title]",
              "param_author" : "1"
            },
            "PROVIDE" : { "entity_created" : { "certificate_entity_created" : "Created Certificate entity" } }
          }
        },
        { "data_set" : {
            "data" : [ "certificate-entity-created:field-date-completed" ],
            "value" : "now"
          }
        },
        { "data_set" : {
            "data" : [ "certificate-entity-created:field-member" ],
            "value" : [ "user" ]
          }
        },
        { "entity_save" : { "data" : [ "certificate-entity-created" ] } }
      ]
    }
  }');
  return $items;
}
