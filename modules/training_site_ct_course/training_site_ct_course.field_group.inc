<?php
/**
 * @file
 * training_site_ct_course.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function training_site_ct_course_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_button_tab|node|course|form';
  $field_group->group_name = 'group_course_button_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_course_options_tabgroup';
  $field_group->data = array(
    'label' => 'Button image',
    'weight' => '5',
    'children' => array(
      0 => 'field_course_button_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-course-button-tab field-group-fieldset ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_course_button_tab|node|course|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_description_tab|node|course|form';
  $field_group->group_name = 'group_course_description_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_course_options_tabgroup';
  $field_group->data = array(
    'label' => 'Description',
    'weight' => '4',
    'children' => array(
      0 => 'body',
      1 => 'field_course_image',
      2 => 'title',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-course-description-tab field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_course_description_tab|node|course|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_expires_tab|node|course|form';
  $field_group->group_name = 'group_course_expires_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_course_options_tabgroup';
  $field_group->data = array(
    'label' => 'Expires',
    'weight' => '7',
    'children' => array(
      0 => 'field_course_certificate_expires',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'formatter' => 'collapsible',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-course-expires-tab field-group-fieldset ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_course_expires_tab|node|course|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_options_tabgroup|node|course|form';
  $field_group->group_name = 'group_course_options_tabgroup';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Course Options',
    'weight' => '2',
    'children' => array(
      0 => 'group_course_description_tab',
      1 => 'group_course_button_tab',
      2 => 'group_course_expires_tab',
      3 => 'group_course_quiz_tab',
    ),
    'format_type' => 'htabs',
    'format_settings' => array(
      'formatter' => '',
      'instance_settings' => array(
        'classes' => 'group-course-options-tabgroup field-group-htabs ',
      ),
    ),
  );
  $export['group_course_options_tabgroup|node|course|form'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_course_quiz_tab|node|course|form';
  $field_group->group_name = 'group_course_quiz_tab';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'course';
  $field_group->mode = 'form';
  $field_group->parent_name = 'group_course_options_tabgroup';
  $field_group->data = array(
    'label' => 'Quiz',
    'weight' => '6',
    'children' => array(
      0 => 'field_course_quiz_ref',
    ),
    'format_type' => 'htab',
    'format_settings' => array(
      'formatter' => 'closed',
      'instance_settings' => array(
        'description' => '',
        'classes' => 'group-course-quiz-tab field-group-htab ',
        'required_fields' => 1,
      ),
    ),
  );
  $export['group_course_quiz_tab|node|course|form'] = $field_group;

  return $export;
}
