<?php
/**
 * @file
 * training_site_ct_course.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function training_site_ct_course_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function training_site_ct_course_node_info() {
  $items = array(
    'course' => array(
      'name' => t('Course'),
      'base' => 'node_content',
      'description' => t('Courses are made up of pages organized like a book with a link to a quiz presented when the last page is viewed.'),
      'has_title' => '1',
      'title_label' => t('Course Title'),
      'help' => '',
    ),
  );
  return $items;
}
