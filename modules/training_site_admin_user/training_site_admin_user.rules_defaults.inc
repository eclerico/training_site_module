<?php
/**
 * @file
 * training_site_admin_user.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function training_site_admin_user_default_rules_configuration() {
  $items = array();
  $items['rules_ts_training_admin_login'] = entity_import('rules_config', '{ "rules_ts_training_admin_login" : {
      "LABEL" : "TS Training Admin Login",
      "PLUGIN" : "reaction rule",
      "TAGS" : [ "Training site" ],
      "REQUIRES" : [ "rules" ],
      "ON" : [ "user_login" ],
      "IF" : [
        { "user_has_role" : { "account" : [ "account" ], "roles" : { "value" : { "4" : "4" } } } }
      ],
      "DO" : [ { "redirect" : { "url" : "admin\\/workbench" } } ]
    }
  }');
  return $items;
}
