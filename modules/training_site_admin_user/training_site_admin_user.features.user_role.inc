<?php
/**
 * @file
 * training_site_admin_user.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function training_site_admin_user_user_default_roles() {
  $roles = array();

  // Exported role: training site admin.
  $roles['training site admin'] = array(
    'name' => 'training site admin',
    'weight' => '3',
  );

  return $roles;
}
