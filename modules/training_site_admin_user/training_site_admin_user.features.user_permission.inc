<?php
/**
 * @file
 * training_site_admin_user.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function training_site_admin_user_user_default_permissions() {
  $permissions = array();

  // Exported permission: access administration pages.
  $permissions['access administration pages'] = array(
    'name' => 'access administration pages',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'system',
  );

  // Exported permission: access all views.
  $permissions['access all views'] = array(
    'name' => 'access all views',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'views',
  );

  // Exported permission: access coffee.
  $permissions['access coffee'] = array(
    'name' => 'access coffee',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'coffee',
  );

  // Exported permission: access content overview.
  $permissions['access content overview'] = array(
    'name' => 'access content overview',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: access printer-friendly version.
  $permissions['access printer-friendly version'] = array(
    'name' => 'access printer-friendly version',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'book',
  );

  // Exported permission: access quiz.
  $permissions['access quiz'] = array(
    'name' => 'access quiz',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'course taker' => 'course taker',
      'training site admin' => 'training site admin',
    ),
    'module' => 'quiz',
  );

  // Exported permission: access site reports.
  $permissions['access site reports'] = array(
    'name' => 'access site reports',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'system',
  );

  // Exported permission: access user profiles.
  $permissions['access user profiles'] = array(
    'name' => 'access user profiles',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'user',
  );

  // Exported permission: access workbench.
  $permissions['access workbench'] = array(
    'name' => 'access workbench',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'workbench',
  );

  // Exported permission: add content to books.
  $permissions['add content to books'] = array(
    'name' => 'add content to books',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'book',
  );

  // Exported permission: administer book outlines.
  $permissions['administer book outlines'] = array(
    'name' => 'administer book outlines',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'book',
  );

  // Exported permission: administer comments.
  $permissions['administer comments'] = array(
    'name' => 'administer comments',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'comment',
  );

  // Exported permission: administer nodes.
  $permissions['administer nodes'] = array(
    'name' => 'administer nodes',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: administer users.
  $permissions['administer users'] = array(
    'name' => 'administer users',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'user',
  );

  // Exported permission: administer workbench.
  $permissions['administer workbench'] = array(
    'name' => 'administer workbench',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'workbench',
  );

  // Exported permission: bypass node access.
  $permissions['bypass node access'] = array(
    'name' => 'bypass node access',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create course content.
  $permissions['create course content'] = array(
    'name' => 'create course content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create course_page content.
  $permissions['create course_page content'] = array(
    'name' => 'create course_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create long_answer content.
  $permissions['create long_answer content'] = array(
    'name' => 'create long_answer content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create matching content.
  $permissions['create matching content'] = array(
    'name' => 'create matching content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create multichoice content.
  $permissions['create multichoice content'] = array(
    'name' => 'create multichoice content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create new books.
  $permissions['create new books'] = array(
    'name' => 'create new books',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'book',
  );

  // Exported permission: create quiz content.
  $permissions['create quiz content'] = array(
    'name' => 'create quiz content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create quiz_directions content.
  $permissions['create quiz_directions content'] = array(
    'name' => 'create quiz_directions content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create short_answer content.
  $permissions['create short_answer content'] = array(
    'name' => 'create short_answer content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create training_group content.
  $permissions['create training_group content'] = array(
    'name' => 'create training_group content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create truefalse content.
  $permissions['create truefalse content'] = array(
    'name' => 'create truefalse content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: create url aliases.
  $permissions['create url aliases'] = array(
    'name' => 'create url aliases',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'path',
  );

  // Exported permission: delete any article content.
  $permissions['delete any article content'] = array(
    'name' => 'delete any article content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any book content.
  $permissions['delete any book content'] = array(
    'name' => 'delete any book content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any course content.
  $permissions['delete any course content'] = array(
    'name' => 'delete any course content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any course_page content.
  $permissions['delete any course_page content'] = array(
    'name' => 'delete any course_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any long_answer content.
  $permissions['delete any long_answer content'] = array(
    'name' => 'delete any long_answer content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any matching content.
  $permissions['delete any matching content'] = array(
    'name' => 'delete any matching content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any multichoice content.
  $permissions['delete any multichoice content'] = array(
    'name' => 'delete any multichoice content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any page content.
  $permissions['delete any page content'] = array(
    'name' => 'delete any page content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any quiz content.
  $permissions['delete any quiz content'] = array(
    'name' => 'delete any quiz content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any quiz results.
  $permissions['delete any quiz results'] = array(
    'name' => 'delete any quiz results',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'quiz',
  );

  // Exported permission: delete any quiz_directions content.
  $permissions['delete any quiz_directions content'] = array(
    'name' => 'delete any quiz_directions content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any short_answer content.
  $permissions['delete any short_answer content'] = array(
    'name' => 'delete any short_answer content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any training_group content.
  $permissions['delete any training_group content'] = array(
    'name' => 'delete any training_group content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete any truefalse content.
  $permissions['delete any truefalse content'] = array(
    'name' => 'delete any truefalse content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: delete revisions.
  $permissions['delete revisions'] = array(
    'name' => 'delete revisions',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any article content.
  $permissions['edit any article content'] = array(
    'name' => 'edit any article content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any book content.
  $permissions['edit any book content'] = array(
    'name' => 'edit any book content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any course content.
  $permissions['edit any course content'] = array(
    'name' => 'edit any course content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any course_page content.
  $permissions['edit any course_page content'] = array(
    'name' => 'edit any course_page content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any long_answer content.
  $permissions['edit any long_answer content'] = array(
    'name' => 'edit any long_answer content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any matching content.
  $permissions['edit any matching content'] = array(
    'name' => 'edit any matching content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any multichoice content.
  $permissions['edit any multichoice content'] = array(
    'name' => 'edit any multichoice content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any page content.
  $permissions['edit any page content'] = array(
    'name' => 'edit any page content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any quiz content.
  $permissions['edit any quiz content'] = array(
    'name' => 'edit any quiz content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any quiz_directions content.
  $permissions['edit any quiz_directions content'] = array(
    'name' => 'edit any quiz_directions content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any short_answer content.
  $permissions['edit any short_answer content'] = array(
    'name' => 'edit any short_answer content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any training_group content.
  $permissions['edit any training_group content'] = array(
    'name' => 'edit any training_group content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit any truefalse content.
  $permissions['edit any truefalse content'] = array(
    'name' => 'edit any truefalse content',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: edit question titles.
  $permissions['edit question titles'] = array(
    'name' => 'edit question titles',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'quiz',
  );

  // Exported permission: publish button publish any content types.
  $permissions['publish button publish any content types'] = array(
    'name' => 'publish button publish any content types',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: publish button unpublish any content types.
  $permissions['publish button unpublish any content types'] = array(
    'name' => 'publish button unpublish any content types',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'publish_button',
  );

  // Exported permission: revert revisions.
  $permissions['revert revisions'] = array(
    'name' => 'revert revisions',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: use text format filtered_html.
  $permissions['use text format filtered_html'] = array(
    'name' => 'use text format filtered_html',
    'roles' => array(
      'administrator' => 'administrator',
      'authenticated user' => 'authenticated user',
      'training site admin' => 'training site admin',
    ),
    'module' => 'filter',
  );

  // Exported permission: view any quiz question correct response.
  $permissions['view any quiz question correct response'] = array(
    'name' => 'view any quiz question correct response',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'quiz',
  );

  // Exported permission: view any quiz results.
  $permissions['view any quiz results'] = array(
    'name' => 'view any quiz results',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'quiz',
  );

  // Exported permission: view quiz question outside of a quiz.
  $permissions['view quiz question outside of a quiz'] = array(
    'name' => 'view quiz question outside of a quiz',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'quiz',
  );

  // Exported permission: view revisions.
  $permissions['view revisions'] = array(
    'name' => 'view revisions',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'node',
  );

  // Exported permission: view the administration theme.
  $permissions['view the administration theme'] = array(
    'name' => 'view the administration theme',
    'roles' => array(
      'administrator' => 'administrator',
      'training site admin' => 'training site admin',
    ),
    'module' => 'system',
  );

  return $permissions;
}
