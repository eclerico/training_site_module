<?php
/**
 * @file
 * training_site_ct_course_page.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function training_site_ct_course_page_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function training_site_ct_course_page_node_info() {
  $items = array(
    'course_page' => array(
      'name' => t('Course page'),
      'base' => 'node_content',
      'description' => t('Add pages to a Course which will be reviewed prior to taking the Quiz.'),
      'has_title' => '1',
      'title_label' => t('Page title'),
      'help' => '',
    ),
  );
  return $items;
}
