<?php
/**
 * @file
 * training_site_config.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function training_site_config_user_default_roles() {
  $roles = array();

  // Exported role: course taker.
  $roles['course taker'] = array(
    'name' => 'course taker',
    'weight' => '4',
  );

  return $roles;
}
