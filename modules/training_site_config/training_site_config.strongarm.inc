<?php
/**
 * @file
 * training_site_config.strongarm.inc
 */

/**
 * Implements hook_strongarm().
 */
function training_site_config_strongarm() {
  $export = array();

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_certificate_of_completion';
  $strongarm->value = array(
    'view_own' => array(
      0 => 2,
      1 => 3,
      2 => 4,
    ),
    'view' => array(
      0 => 2,
      1 => 3,
      2 => 4,
    ),
  );
  $export['content_access_certificate_of_completion'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_course';
  $strongarm->value = array(
    'view_own' => array(
      0 => 2,
      1 => 4,
      2 => 3,
      3 => 5,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 4,
      3 => 3,
      4 => 5,
    ),
  );
  $export['content_access_course'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_course_page';
  $strongarm->value = array(
    'view_own' => array(
      0 => 2,
      1 => 3,
      2 => 4,
    ),
    'view' => array(
      0 => 2,
      1 => 3,
      2 => 4,
    ),
  );
  $export['content_access_course_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_page';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
    ),
  );
  $export['content_access_page'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_quiz';
  $strongarm->value = array(
    'view_own' => array(
      0 => 2,
      1 => 3,
      2 => 4,
    ),
    'view' => array(
      0 => 2,
      1 => 3,
      2 => 4,
    ),
  );
  $export['content_access_quiz'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'content_access_training_group';
  $strongarm->value = array(
    'view_own' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
    ),
    'view' => array(
      0 => 1,
      1 => 2,
      2 => 3,
      3 => 4,
    ),
  );
  $export['content_access_training_group'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_access_denied_message';
  $strongarm->value = 'Access denied. You must log in to view this page.';
  $export['r4032login_access_denied_message'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_access_denied_message_type';
  $strongarm->value = 'error';
  $export['r4032login_access_denied_message_type'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_default_redirect_code';
  $strongarm->value = '302';
  $export['r4032login_default_redirect_code'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_display_denied_message';
  $strongarm->value = 1;
  $export['r4032login_display_denied_message'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_match_noredirect_pages';
  $strongarm->value = '';
  $export['r4032login_match_noredirect_pages'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_redirect_authenticated_users_to';
  $strongarm->value = '<front>';
  $export['r4032login_redirect_authenticated_users_to'] = $strongarm;

  $strongarm = new stdClass();
  $strongarm->disabled = FALSE; /* Edit this to true to make a default strongarm disabled initially */
  $strongarm->api_version = 1;
  $strongarm->name = 'r4032login_user_login_path';
  $strongarm->value = 'user/login';
  $export['r4032login_user_login_path'] = $strongarm;

  return $export;
}
