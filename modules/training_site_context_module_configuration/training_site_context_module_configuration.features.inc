<?php
/**
 * @file
 * training_site_context_module_configuration.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function training_site_context_module_configuration_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
}
