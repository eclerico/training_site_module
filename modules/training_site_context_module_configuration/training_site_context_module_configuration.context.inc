<?php
/**
 * @file
 * training_site_context_module_configuration.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function training_site_context_module_configuration_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'course_pages';
  $context->description = 'Places the Angualr block for paging through courses';
  $context->tag = 'structure';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'course' => 'course',
        'course/*' => 'course/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'training_site_course_block-Course_Page_AngularJS_App' => array(
          'module' => 'training_site_course_block',
          'delta' => 'Course_Page_AngularJS_App',
          'region' => 'content',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Places the Angualr block for paging through courses');
  t('structure');
  $export['course_pages'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'login_with_facebook';
  $context->description = 'It\'s Dr. Beard Facé!';
  $context->tag = 'structure';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'facebook' => 'facebook',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'hybridauth-hybridauth' => array(
          'module' => 'hybridauth',
          'delta' => 'hybridauth',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('It\'s Dr. Beard Facé!');
  t('structure');
  $export['login_with_facebook'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'site_wide_context';
  $context->description = 'SIte wide context places content, footer, navbar etc';
  $context->tag = 'structure';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'system-main-menu' => array(
          'module' => 'system',
          'delta' => 'main-menu',
          'region' => 'navigation',
          'weight' => '-10',
        ),
        'system-help' => array(
          'module' => 'system',
          'delta' => 'help',
          'region' => 'help',
          'weight' => '-10',
        ),
        'workbench-block' => array(
          'module' => 'workbench',
          'delta' => 'block',
          'region' => 'content',
          'weight' => '-10',
        ),
        'system-main' => array(
          'module' => 'system',
          'delta' => 'main',
          'region' => 'content',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('SIte wide context places content, footer, navbar etc');
  t('structure');
  $export['site_wide_context'] = $context;

  return $export;
}
